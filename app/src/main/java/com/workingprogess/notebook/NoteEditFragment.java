package com.workingprogess.notebook;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class NoteEditFragment extends Fragment {

    private ImageButton noteCatButton;
    private EditText title;
    private EditText message;
    private Button saveButton;
    private Note.Category savedButtonCategory;
    private AlertDialog categoryDialogObject;
    private AlertDialog confirmDialogObject;
    private boolean newNote =false;

    private static final String MODIFIED_CATEGORY="Modified Category";

    private long noteId=0;


    public NoteEditFragment() {
        // Required empty public constructor
    }

    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //grab bundle sent with fragment
        Bundle bundle =this.getArguments();
        if (bundle !=null){
            newNote = bundle.getBoolean(NoteDetailActivity.NEW_NOTE_EXTRA,false);
        }

        if(savedInstanceState!=null){
            savedButtonCategory = (Note.Category) savedInstanceState.get(MODIFIED_CATEGORY);
        }

        //grab layout and assign to view so that we may access widgets
        View fragmentLayout = inflater.inflate(R.layout.fragment_note_edit, container, false);

        //grab widget references
        title = (EditText) fragmentLayout.findViewById(R.id.editNoteTitle);
        message = (EditText) fragmentLayout.findViewById(R.id.editMessage);
        noteCatButton = (ImageButton) fragmentLayout.findViewById(R.id.editNoteButton);
        saveButton = (Button) fragmentLayout.findViewById(R.id.saveNoteButton);


        //populate with note data
        Intent intent = getActivity().getIntent();

        title.setText(intent.getExtras().getString(MainActivity.NOTE_TITLE_EXTRA,""));
        message.setText(intent.getExtras().getString(MainActivity.NOTE_MESSAGE_EXTRA,""));
        noteId = intent.getExtras().getLong(MainActivity.NOTE_ID_EXTRA,0);


        if(savedButtonCategory !=null){
            noteCatButton.setImageResource(Note.categoryToDrawable(savedButtonCategory));
        } else if(!newNote) {
            Note.Category noteCat = (Note.Category) intent.getSerializableExtra(MainActivity.NOTE_CATEGORY_EXTRA);
            savedButtonCategory = noteCat;
            noteCatButton.setImageResource(Note.categoryToDrawable(noteCat));
        }



//set onclick listeners
        buildCategoryDialog();
        noteCatButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                categoryDialogObject.show();
            }
        });

        buildConfirmDialog();
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmDialogObject.show();
            }
        });



        // Inflate the layout for this fragment
        return fragmentLayout;
    }

    //save info before orientation change.
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putSerializable(MODIFIED_CATEGORY, savedButtonCategory);

    }

    //build pop uo dialog to change note info
    private void buildCategoryDialog(){

        final String[] categories = new String[]{"Personal","Technical","Quote","Finance"};
        final AlertDialog.Builder  categoryBuilder = new AlertDialog.Builder(getActivity());
        categoryBuilder.setTitle("Choose Note Type");

        categoryBuilder.setSingleChoiceItems(categories, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int item) {

                switch (item){
                    case 0:
                        savedButtonCategory= Note.Category.PERSONAL;
                        noteCatButton.setImageResource(R.drawable.p_icon);
                        break;
                    case 1:
                        savedButtonCategory=Note.Category.TECHNICAL;
                        noteCatButton.setImageResource(R.drawable.t_icon);
                        break;
                    case 2:
                        savedButtonCategory=Note.Category.QUOTE;
                        noteCatButton.setImageResource(R.drawable.q_icon);
                        break;
                    case 3:
                        savedButtonCategory=Note.Category.FINANCE;
                        noteCatButton.setImageResource(R.drawable.f_icon );
                        break;

                }
                categoryDialogObject.cancel();

            }
        });

        categoryDialogObject=categoryBuilder.create();

    }

    private void buildConfirmDialog(){
        final AlertDialog.Builder confirmBuilder = new AlertDialog.Builder(getActivity());
        confirmBuilder.setTitle("are you sure?");
        confirmBuilder.setMessage("are you sure you want to save this note");

        confirmBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("Save Note","Note title " + title.getText()+ "Note message "
                    + message.getText()+" note category" + savedButtonCategory);


                NotebookDbAdapter dbAdapter = new NotebookDbAdapter((getActivity().getBaseContext()));
                dbAdapter.open();

                //if new note add to database otherwise update database
                if (newNote){
                    dbAdapter.createNote(title.getText()+"",message.getText()+"",(savedButtonCategory==null)? Note.Category.PERSONAL : savedButtonCategory);

                }else{
                    dbAdapter.updateNote(noteId,title.getText() +"",message.getText()+"",savedButtonCategory);
                    Log.d("test2 " ," "+ title.getText() + "note " + noteId);

                }
                Intent intent = new Intent(getActivity(),MainActivity.class);
                startActivity(intent);

                dbAdapter.close();


            }
        });
        confirmBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                savedButtonCategory=Note.Category.TECHNICAL;


            }
        });

        confirmDialogObject = confirmBuilder.create();

    }

}
