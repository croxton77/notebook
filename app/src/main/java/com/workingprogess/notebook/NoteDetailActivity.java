package com.workingprogess.notebook;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NoteDetailActivity extends AppCompatActivity {

    public static final String NEW_NOTE_EXTRA ="new note";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);
        createAndAddFragment();
    }

    private void createAndAddFragment(){

        //grab intent and extras from intent
        Intent intent =getIntent();
        MainActivity.FragmentToLaunch fragmentToLaunch = (MainActivity.FragmentToLaunch)
                intent.getSerializableExtra(MainActivity.NOTE_FRAGMENT_TO_LOAD_EXTRA);

        //grab fragment manager and fragment tramsaction so that we can add view dynamically
        FragmentManager  fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //choose correct fragment to load
        switch(fragmentToLaunch){
            case EDIT:
                //create and add note edit fragment
                NoteEditFragment noteEditFragment = new NoteEditFragment();
                setTitle(R.string.note_edit_title);
                fragmentTransaction.add(R.id.note_container, noteEditFragment,"Note_Edit_Fragment");
                break;
            case VIEW:
                //create and add note view fragment
                NoteViewFragment noteViewFragment = new NoteViewFragment();
                setTitle(R.string.noteFragmentTitle);
                fragmentTransaction.add(R.id.note_container, noteViewFragment,"Note_View_Fragment");
                break;
            case CREATE:
                //create and edit new note
                NoteEditFragment noteCreateFragment = new NoteEditFragment();
                setTitle(R.string.create_note_tile);

                Bundle bundle = new Bundle();
                bundle.putBoolean(NEW_NOTE_EXTRA, true);
                noteCreateFragment.setArguments(bundle);

                fragmentTransaction.add(R.id.note_container,noteCreateFragment,"Note_Create_Fragment");
                break;

        }



        fragmentTransaction.commit();
    }
}
