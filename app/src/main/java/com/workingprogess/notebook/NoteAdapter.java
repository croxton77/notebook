package com.workingprogess.notebook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Calvin on 8/18/2016.
 */
public class NoteAdapter extends ArrayAdapter<Note> {

    public static class ViewHolder{
        TextView title;
        TextView body;
        ImageView icon;
    }


    public NoteAdapter(Context context, ArrayList<Note> notes){

        super(context,0,notes);
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        //get data item for postion
        Note note = getItem(position);

        //create new viewholder
        ViewHolder viewHolder;

        // check if an existing view is being reuused ptherwise inflates ne view
        if (convertView == null) {
            viewHolder = new ViewHolder();

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listrow, parent, false);

            viewHolder.title = (TextView) convertView.findViewById(R.id.listItemNoteTitle);
            viewHolder.body  = (TextView) convertView.findViewById(R.id.listitemNoteBody);
            viewHolder.icon =(ImageView) convertView.findViewById(R.id.listItemNoteImg);

            //use settag to remeber viewholder hich is holding references to widgets
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }



        //fill each new referemced view with data associated with note its referencing
        viewHolder.title.setText(note.getTitle());
        viewHolder.body.setText(note.getMessage() );
        viewHolder.icon.setImageResource(note.getAssociatedDrawable());

        return convertView;
    }

}
