package com.workingprogess.notebook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Calvin on 9/5/2016.
 */
public class NotebookDbAdapter {


    private static final String DATABASE_NAME = "notebook.db";
    private static final int DATABASE_VERSION = 1;

    public static final String NOTE_TABLE = "note";
    public static final String COLOUMN_ID = "_id";
    public static final String COLOUMN_TITLE = "title";
    public static final String COLOUMN_MESSAGE = "message";
    public static final String COLOUMN_CATEGORY = "category";
    public static final String COULOUMN_DATE = "date";

    private String[] allColumns = {COLOUMN_ID, COLOUMN_TITLE, COLOUMN_MESSAGE, COLOUMN_CATEGORY, COULOUMN_DATE};

    public static final String CREATE_TABLE_NOTE = "create table " + NOTE_TABLE + " ( "
            + COLOUMN_ID + " integer primary key autoincrement, "
            + COLOUMN_TITLE + " text not null, "
            + COLOUMN_MESSAGE + " text not null, "
            + COLOUMN_CATEGORY + " text not null, "
            + COULOUMN_DATE + ");";

    private SQLiteDatabase sqlDB;
    private Context context;
    private NotebookDbHelper notebookDbHelper;

    public NotebookDbAdapter(Context ctx) {
        context = ctx;
    }

    //opens database and assigns to local sqldb variable
    public NotebookDbAdapter open() throws android.database.SQLException {
        notebookDbHelper = new NotebookDbHelper(context);
        sqlDB = notebookDbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        notebookDbHelper.close();
    }

    public Note createNote(String title, String message, Note.Category category){
        //function used to add note to database

        ContentValues values = new ContentValues();
        values.put(COLOUMN_TITLE, title);
        values.put(COLOUMN_MESSAGE,message);
        values.put(COLOUMN_CATEGORY,category.name());
        values.put(COULOUMN_DATE, Calendar.getInstance().getTimeInMillis()+"");

        long insertId = sqlDB.insert(NOTE_TABLE, null, values);

        Cursor cursor = sqlDB.query(NOTE_TABLE,allColumns,COLOUMN_ID + " = " + insertId,null,null,null,null);
        cursor.moveToFirst();

        Note newNote = cursorToNote(cursor);
        return newNote;

    }

    public long deleteNote(long idToDelete){
        return sqlDB.delete(NOTE_TABLE,COLOUMN_ID + " = " + idToDelete, null);
    }

    public long updateNote(long idToUpdate,String newTitle,String newMessage, Note.Category newCategory){
        ContentValues values = new ContentValues();
        values.put(COLOUMN_TITLE, newTitle);
        values.put(COLOUMN_MESSAGE,newMessage);
        values.put(COLOUMN_CATEGORY,newCategory.name());
        values.put(COULOUMN_DATE, Calendar.getInstance().getTimeInMillis()+"");


        Log.d("update","code is reached");
        return sqlDB.update(NOTE_TABLE, values, COLOUMN_ID + " = " + idToUpdate,null);


    }


    public ArrayList<Note> getAllnotes(){
        ArrayList<Note> notes = new ArrayList<Note>();

        //grab all info in our database for the notes in it
        Cursor cursor = sqlDB.query(NOTE_TABLE,allColumns,null,null,null,null,null);

        for (cursor.moveToLast(); !cursor.isBeforeFirst(); cursor.moveToPrevious()){
            Note note = cursorToNote(cursor);
            notes.add(note);
        }
        cursor.close();
        return notes;
    }

    private Note cursorToNote(Cursor cursor){
        Note newNote = new Note(cursor.getString(1),cursor.getString(2),
                Note.Category.valueOf(cursor.getString(3)),cursor.getLong(0),cursor.getLong(4));
        return newNote;
    }


    private static class NotebookDbHelper extends SQLiteOpenHelper{

        NotebookDbHelper(Context ctx){

            super(ctx, DATABASE_NAME,null,DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db){
            //create note table
            db.execSQL(CREATE_TABLE_NOTE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(NotebookDbHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
            //destroys data and create new table
            db.execSQL("DROP TABLE IF EXISTS " + NOTE_TABLE);
            onCreate(db);
        }
    }
}







