package com.workingprogess.notebook;

/**
 * Created by Calvin on 8/18/2016.
 */
public class Note {
    //this class contains note data structure

    private String title, message;
    private long noteId, dateCreatedMilli;
    private Category category;

    public enum Category{ PERSONAL,TECHNICAL,QUOTE,FINANCE}

        //constructor below
        public Note(String title, String message, Category category){
            //initialize variables
            this.title = title;
            this.message = message;
            this.category = category;
            this.noteId = 0;
            this.dateCreatedMilli = 0;
        }
    public Note(String title, String message, Category category,long noteID, long dateCreatedMilli){
        //overload allow input of all variables upon creation or pulled from other source.
        this.title = title;
        this.message = message;
        this.category= category;
        this.noteId = noteID;
       // this.dateCreatedMilli = dateCreatedMilli;


    }

    public String getTitle(){
        return title;
    }

    public String getMessage(){
        return message;
    }

    public Category getCategory(){
        return category;
    }

    public long getDate(){
        return dateCreatedMilli;
    }
    public long getId(){
        return noteId;
    }

    public String toString(){
        return "Id: " + noteId + " Title: "+ title + " Message: "+ message +" IconID: " + category.name() +" Date: ";
    }

    public int getAssociatedDrawable(){
        return categoryToDrawable(category);
    }

    public static int categoryToDrawable(Category noteCategory){
        switch(noteCategory){
            case PERSONAL:
                return R.drawable.p_icon;
            case TECHNICAL:
                return R.drawable.t_icon;
            case FINANCE:
                return R.drawable.f_icon;
            case QUOTE:
                return R.drawable.q_icon;
        }
        return R.drawable.p_icon;
    }
}
